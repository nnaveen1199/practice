from flask import Flask,request,jsonify
import  pymysql
app = Flask(__name__)
conn=pymysql.connect( host='localhost',
                      user= 'root',
                      password='',
                      database='employee')
@app.route('/insert_query/<table_name>/<data>',methods=['POST'])
def insert_qry(table_name,data):#={'emp_id': 4, 'name': 'siva', 'salary_grade': 'a', 'address': 'trichy'}):
    if request.method == 'POST':
        cur = conn.cursor()
        qry = "INSERT INTO " + table_name + " VALUES {}"
        #cols = ''
        values = "{}".format(data)
        #for key in data:
            #cols += "{},".format(key)
            #values += "{},".format(data[key])
        query = qry.format(values)#(cols[:-1], values[:-1])
        cur.execute(query)
        conn.commit()
        cur.close()
        return f'done'
@app.route('/select_query/<table_name>',methods=['GET'])
def select_query(table_name):
    if request.method=='GET':
        #table_name=request.form['table_name']
        cursor=conn.cursor()
        query="SELECT * FROM "+table_name
        cursor.execute(query)#('''SELECT * FROM (%s)''',(table_name))
        result=cursor.fetchall()
        cursor.close()
        return jsonify({'data':result})
#@app.route('/update_query/<name>',methods=['POST','GET'])
@app.route('/update_query/<table_name>/<value>/<data>',methods=['POST','GET'])
def update_query(table_name,value,data):
    if request.method == 'POST':
        cur = conn.cursor()
        qry = "UPDATE  " + table_name + " SET {0} WHERE emp_id={1};"
        #update = ''
        #update1=''
        #for key in column:
            #update += "{}=\"{}\",".format(key,column[key])
        #for key1 in data:
            #update1 +="{}=\"{}\",".format(key1,data[key1])
        #col="{}".format(column_name)
        update="{}".format(value)
        update1="{}".format(data)
        query = qry.format(update,update1)
        cur.execute(query)
        conn.commit()
        cur.close()
        return f'done'
    #if request.method=='POST':
        #emp_id = request.form['emp_id']
        #name = request.form['name']
        #salary_grade = request.form['salary_grade']
        #address = request.form['address']
        #cursor=conn.cursor()
        #query='''UPDATE {0} SET name= {0} WHERE emp_id={1}'''.format(table_name,name,emp_id)
        #cursor.execute(query)
        #conn.commit()
        #cursor.close()
        #return f'ok'
@app.route('/delete_query/<table_name>/<emp_id>',methods=['DELETE'])
def delete_query(table_name,emp_id):
    if request.method=='DELETE':
        #emp_id=request.form['emp_id']
        cursor=conn.cursor()
        query="DELETE FROM {0} WHERE emp_id={1}".format(table_name,emp_id)
        cursor.execute(query)
        conn.commit()
        cursor.close()
        return f'deleted successfully'
app.run()
