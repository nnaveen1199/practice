from flask import Flask, render_template, request, jsonify
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'employee'

mysql = MySQL(app)


# table_name='employee_table'
# data={'emp_id':3,'name':'ram','salary_grade':'a','address':'karur'}

#@app.route('/form')
#def form():
    #print("1")
    #return render_template('form.html')


@app.route('/insert_qry', methods=['POST', 'GET'])
def insert_qry(table_name='employee_table', data={'emp_id': 4, 'name': 'siva', 'salary_grade': 'a', 'address': 'trichy'}):
    if request.method == 'POST':
        cur = mysql.connection.cursor()
        qry = "INSERT INTO " + table_name + " ({0}) VALUES ({1})"
        cols = ''
        values = ''
        for key in data:
            cols += "{},".format(key)
            values += "'{}',".format(data[key])
        query = qry.format(cols[:-1], values[:-1])
        cur.execute(query)
        mysql.connection.commit()
        cur.close()
        return f'done'


@app.route('/update_qry', methods=['POST', 'GET'])
def update_qry(table_name='employee_table', column=None, data=None):
    if data is None:
        data = {'emp_id': 2}
    if column is None:
        column = {'name': 'ragul'}
    if request.method == 'POST':
        cur = mysql.connection.cursor()
        qry = "UPDATE  " + table_name + "SET ({0}) WHERE ({1});"
        update = ''
        update1=''
        for key in column:
            update += "{}=\"{}\",".format(key,column[key])
        for key1 in data:
            update1 +="{}=\"{}\",".format(key1,data[key1])
        query = qry.format(update[:-1],update1[:-1])
        cur.execute(query)
        mysql.connection.commit()
        cur.close()
        return f'done'


@app.route('/select_qry', methods=['POST', 'GET'])
def select_qry(table_name='employee_table'):
    if request.method == 'GET':
        cur = mysql.connection.cursor()
        qry = "SELECT * FROM " + table_name
        cur.execute(qry)
        result = cur.fetchall()
        result1=[result]
        cur.close()
        return jsonify({"data": result})


@app.route('/delete_qry', methods=['POST', 'GET', 'DELETE'])
def delete_qry(table_name='employee_table', data={'emp_id':2}):
    if request.method == 'DELETE':
        cur = mysql.connection.cursor()
        qry = '''DELETE FROM''' + table_name + ''' WHERE emp_id={0}'''.format(data)
        delete = ''
        for key in data:
            delete += "{}=\"{}\",".format(key, data[key])
        query = qry + delete[:-1]
        cur.execute(query)
        mysql.connection.commit()
        cur.close()
        return f'done'


app.run()
#d= insert_qry('employee_table',{'emp_id':3,'name':'ram','salary_grade':'a','address':'karur'})
